
const response = require('../middleware/res.middleware');
const connection = require('../config/db.config');

exports.getDate = function (value) {
    let date_ob = new Date(value);
    let date = ("0" + date_ob.getDate()).slice(-2);
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    let year = date_ob.getFullYear();

    return year + "-" + month + "-" + date;
};

exports.ValidatePassword = function (password){
    var isValid = true;
    var minNumberofChars = 6;
    var maxNumberofChars = 32;
    var regularExpression  = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,32}$/;

    if(password.length < minNumberofChars || password.length > maxNumberofChars){
        isValid = false;
    }
    if(!regularExpression.test(password)) {
        isValid = false
    }

    return isValid;
};

exports.inMonth = function(quarter){
    var arrMonth = [];
    if(quarter === '1'){
        arrMonth = ["1", "2", "3"]
    }
    
    if(quarter === '2'){
        arrMonth = ["4", "5", "6"]
    }
    
    if(quarter === '3'){
        arrMonth = ["7", "8", "9"]
    }
    
    if(quarter === '4'){
        arrMonth = ["10", "11", "12"]
    }

    return arrMonth;
};