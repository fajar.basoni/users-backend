'use strict';

const response = require('../middleware/res.middleware');
const connection = require('../config/db.config');
const common = require('../controllers/common.controller');
const secretKey = require('../config/auth.config');
const jwt = require('jsonwebtoken');

exports.index = function (req, res) {
    response.send(200, "Hello from the Node JS RESTful side!", res)
};

exports.getToken = function (req, res) {
    // validate 
    if (!req.body.user) {
        return response.error(400, "Invalid input!!", res);
    }

    var user = req.body.user;

    connection.query('SELECT * FROM users WHERE username = ? OR email = ?',
        [user, user],
        function (error, rows) {
            if (error) {
                response.send(500, error, res)
            }
            else {
                if (rows.length > 0) {
                    jwt.sign({ user }, secretKey, {
                        expiresIn: '1h'
                    }, (error, token) => {
                        if (error) {
                            response.error(400, error, res);
                        }
                        else {
                            response.token(200, rows, token, res);
                        }
                    })
                } else {
                    response.error(202, "session not exist!", res);
                }
            }
        });
};

exports.getLogin = function (req, res) {
    //Validate 
    if (!req.body.user
        || !req.body.pass) {
        return response.error(400, "Invalid Input", res);
    }

    var user = req.body.user;
    var pass = req.body.pass;

    connection.query('SELECT * FROM users WHERE username = ? OR email = ?',
        [user, user],
        function (error, rows) {
            if (error) {
                response.send(500, error, res)
            }
            else {
                if (rows.length > 0) {
                    if (pass === rows[0].password) {
                        jwt.sign({ user }, secretKey, {
                            expiresIn: '1h'
                        }, (error, token) => {
                            if (error) {
                                response.error(400, error, res);
                            }
                            else {
                                response.token(200, rows, token, res);
                            }
                        })
                    } else {
                        response.error(201, "username and password is not match!!", res);
                    }
                }
                else {
                    response.error(202, "username not exist!", res);
                }
            }
        });
};

exports.getUsers = function (req, res) {
    connection.query('SELECT * FROM users',
        function (error, rows) {
            if (error) {
                response.error(400, error, res)
            } else {
                response.send(200, rows, res)
            }
        });
};

exports.createUsers = function (req, res) {

    var fullName = req.body.fullName;
    var birthday = req.body.birthday;
    var email = req.body.email;
    var username = req.body.username;
    var password = req.body.password;

    // validate not null
    if (!req.body.fullName
        || !req.body.birthday
        || !req.body.email
        || !req.body.username
        || !req.body.password) {
        return response.send(400, "Invalid Input!!", res);
    }

    //validate password
    if (!common.ValidatePassword(password)) {
        return response.error(401, "The password’s length must be between 6-32 characters and must have at least one uppercase letter, one lowercase letter, one number, and one special character", res);
    }

    connection.query('SELECT * FROM users WHERE username = ? OR email = ?', 
    [username, email],
    function (error, rows){
        if(error){
            return response.error(500, error, res);
        } else {
            if(rows.length > 0){
                return response.error(402, "username and email is already exist", res);
            } 
            else {
                connection.query('INSERT INTO users (fullName, birthday, email, username, password) values (?,?,?,?,?)',
                    [fullName, birthday, email, username, password],
                    function (error, rows) {
                        if (error) {
                            response.error(500, error, res);
                        } else {
                            response.send(200, "Register users success!", res)
                        }
                    });
            }
        }
    });
};