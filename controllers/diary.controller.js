'use strict';

const response = require('../middleware/res.middleware');
const connection = require('../config/db.config');
const common = require('../controllers/common.controller');

exports.getDiary = function (req, res) {

    // validate
    if (!req.body.year
        || !req.body.quarter
        || !req.body.username) {
        return response.error(400, "Invalid input!!", res);
    }

    var year = req.body.year;
    var quarter = req.body.quarter;
    var username = req.body.username;

    var inMonth = common.inMonth(quarter);

    connection.query('SELECT diary, createdDate FROM diary WHERE MONTH(createdDate) IN (?) AND YEAR(createdDate) = ? AND createdBy = ? ORDER BY createdDate DESC',
        [inMonth, year, username],
        function (error, rows) {
            if (error) {
                response.error(400, error, res)
            } else {
                response.send(200, rows, res)
            }
        });
};

exports.saveDiary = function (req, res) {
    // validate not null
    if (!req.body.diary
        || !req.body.createdDate
        || !req.body.createdBy) {
        return response.send(400, "Invalid Input!!", res);
    }

    var diary = req.body.diary;
    var createdDate = req.body.createdDate;
    var createdBy = req.body.createdBy;

    connection.query('SELECT * FROM diary WHERE createdDate = ? AND createdBy = ?',
    [common.getDate(createdDate), createdBy],
    function (error, rows){
        if(error){
            return response.error(500, error, res);
        }
        else{
            if(rows.length > 0){
                var Id = rows[0].id;
                connection.query('UPDATE diary SET diary = ?, createdDate = ?, createdBy = ? WHERE id = ?',
                [diary, common.getDate(createdDate), createdBy, Id],
                function (error){
                    if (error) {
                        return response.error(500, error, res);
                    } else {
                        return response.send(200, "Save diary data is success!!", res)
                    }
                })
            }
            else {
                connection.query('INSERT INTO diary (diary, createdDate, createdBy) values (?,?,?)',
                    [diary, common.getDate(createdDate), createdBy],
                    function (error, rows) {
                        if (error) {
                            return response.error(500, error, res);
                        } else {
                            return response.send(200, "Save diary data is success!!", res)
                        }
                    });
            }
        }
    })
};