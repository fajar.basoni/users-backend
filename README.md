# Node and MySql Backend 

## Technology used:
* Node
* MySql
* jwt authentication
* Docker

## How to run?
* Run `npm install` to install all node dependencies.
* Run your database server.
* Execute sql script on your mysql database editor find file in ~/script-db/fajar_basoni.sql
* Run `npm start` or `node server.js` using another terminal on project directory.
* **You are done.** You can go `http://localhost:3000` and check if everything is up and running.

## How to setup in docker?
* Run `npm install` to install all node dependencies.
* Execute sql script on your mysql database editor find file in ~/script-db/fajar_basoni.sql
* Run `docker-compose up` in a terminal. With this command you will have mysql in your environment.
* Run `npm start` or `node server.js` using another terminal on project directory.
* **You are done.** You can go `http://localhost:3000` and check if everything is up and running.

## How to test?
* You can test the api using postman and other tools.
* Read the documentation for the API here : https://documenter.getpostman.com/view/11356963/SzzhdxqQ?version=latest as user guide.