const response = require('../middleware/res.middleware');
const jwt = require('jsonwebtoken');
const secretKey = require('../config/auth.config');

const verifyToken = (req, res, next) => {
    const header = req.headers['authorization'];

    if (!header) {
        return response.send(403, "No token provided!!", res);
    }

    const bearer = header.split(' ');
    const token = bearer[1];

    jwt.verify(token, secretKey, (err, decode) => {
        if(err){
            return response.send(401, "Session timeout!!", res);
        }
        req.token = token;
        next();
    });
}

const authJwt = {
    verifyToken
};
module.exports = authJwt;