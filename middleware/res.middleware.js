'use strict';

exports.send = function(status, values, res){
    var data = {
        'status': status,
        'datas': values
    };
    res.status(status);
    res.json(data);
    res.end();
}

exports.error = function(status, values, res){
    var data = {
        'status': status,
        'message': values
    };
    res.status(status);
    res.json(data);
    res.end();
}

exports.token = function(status, values, token, res){
    var data = {
        'status': status,
        'datas': values,
        'accessToken': token
    };
    res.status(status);
    res.json(data);
    res.end();
}