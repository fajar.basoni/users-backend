'use strict';

module.exports = function (app) {
    const users = require('../controllers/user.controller');
    const diary = require('../controllers/diary.controller');
    const auth = require('../middleware/auth.middleware');

    app.route('/')
        .get(users.index);

    app.route('/api/Users/Login')
        .post(users.getLogin);

    app.route('/api/Users/GetToken')
        .post(users.getToken);

    app.route('/api/Users/Get')
        .get([auth.verifyToken, users.getUsers]);

    app.route('/api/Users/Register')
        .post(users.createUsers);

    app.route('/api/Diary/Save')
        .post([auth.verifyToken, diary.saveDiary]);

    app.route('/api/Diary/Get')
        .get([auth.verifyToken, diary.getDiary]);

};